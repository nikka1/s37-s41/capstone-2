const express = require("express");
const router = express.Router();
const productController = require("../controllers/productControllers");
const auth = require("../auth");

// 4. Create product (Admin only)
router.post("/addProduct", auth.verify, (req, res) => {

	const data = auth.decode(req.headers.authorization);
	console.log(data);

	if(data.isAdmin) {
		productController.addProduct(req.body).then(resultFromController => res.send(resultFromController));
	}

	else {
		res.send("Only admins can create a new product")
	}
	
})


// 5. Retrieve all active products
router.get("/activeProducts", (req, res) => {

	productController.getActiveProducts().then(resultFromController => res.send(resultFromController));
});

// 6. Retrieve single product
router.get("/:productId", (req, res) => {

	console.log(req.params.productId)

	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController))
})

// 7. Update a product
router.put("/:productId", auth.verify, (req, res) => {

	const data = {
		productId: req.params.productId,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		updatedProduct: req.body
	}

	productController.updateProduct(data).then(resultFromController =>res.send(resultFromController));
})

// 8. Archive a product
router.put("/:productId/archive", auth.verify, (req, res) => {

	const data = {
		productId: req.params.productId,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	productController.archiveProduct(data).then(resultFromController => res.send(resultFromController))
})


module.exports = router;