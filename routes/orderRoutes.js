const express = require("express");
const router = express.Router();
const orderController = require("../controllers/orderControllers");
const auth = require("../auth");

// 9. Create order (non-admin)
router.post("/checkout", auth.verify, (req, res) => {

	const data = auth.decode(req.headers.authorization);
	console.log(data);

	if(data.isAdmin == false) {
		orderController.createOrder(req.body).then(resultFromController => res.send(resultFromController))
	}

	else {
		res.send("Admins cannot checkout an order")
	}
})



// 10. Retrieve all orders
router.get("/orders", auth.verify, (req,res) => {

	const data = auth.decode(req.headers.authorization);
	console.log(data);


	if(data.isAdmin) {
		orderController.getAllOrders().then(resultFromController => res.send(resultFromController))
	}

	else {
		res.send("Only admins can view all orders")
	}
})



// 11. Retrieve authenticated user's orders
router.get("/myOrders", auth.verify, (req, res) => {

	let data = {
		userId: auth.decode(req.headers.authorization).id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	console.log(data.userId)
	
	if(data.isAdmin == false) {
		orderController.getMyOrders(data).then(resultFromController => res.send(resultFromController))
	}

	else {
		res.send("Admins have no order data")
	}
})


module.exports = router;

