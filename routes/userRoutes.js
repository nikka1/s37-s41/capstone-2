const express = require("express");
const router = express.Router();
const userController = require("../controllers/userControllers");
const auth = require("../auth");


// 1. User registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

module.exports = router;


// 2. User authentication
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});

// 3. Set user as Admin
router.put("/:userId", auth.verify, (req, res) => {

	const data = {
		userId: req.params.userId,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		updatedUser: req.body
	}

	userController.setAdmin(data).then(resultFromController => res.send(resultFromController));
});

