const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const port = 4000;

const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes");
const orderRoutes = require("./routes/orderRoutes");

const app = express();

mongoose.connect("mongodb+srv://admin:admin123@cluster0.jtm9g.mongodb.net/S37-S41?retryWrites=true&w=majority", {

		useNewUrlParser: true,
		useUnifiedTopology: true
});


let db = mongoose.connection;
db.on('error', () => console.error.bind(console, 'Connection error'));
db.once('open', () => console.log('Now connected to MongoDB Atlas'));


app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use("/users", userRoutes, orderRoutes);
app.use("/products", productRoutes);

app.listen(process.env.PORT || port, () => {
	console.log(`API is now online on port ${process.env.PORT || port}`)
});