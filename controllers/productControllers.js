const Product = require("../models/Product");
const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");


// 4. Create product (Admin only)
module.exports.addProduct = (reqBody) => {

	let newProduct = new Product ({

		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	})

	return newProduct.save().then((product, error) => {

		if(error) {
			return false
		}

		else {
			return true
		}
	})
}

// 5. Retrieve all active products
module.exports.getActiveProducts = () => {

	return Product.find({isActive: true}).then(result => {

		return result
	})
}

// 6. Retrieve single product
module.exports.getProduct = (reqParams) => {

	return Product.findById(reqParams.productId).then(result => {

		return result
	})
}

// 7. Update a product
module.exports.updateProduct = (data) => {

	console.log(data)

	return Product.findById(data.productId).then((result, error) => {

		console.log(result);

		if(data.isAdmin) {
			result.name = data.updatedProduct.name
			result.description = data.updatedProduct.description
			result.price = data.updatedProduct.price

			console.log(result)

			return result.save().then((updatedProduct, error) => {

				if(error) {
					return false
				}

				else {
					return updatedProduct
				}
			})

		}

		else {
			return "Only admins can update a product"
		}

	})
}

// 8. Archiving a product
module.exports.archiveProduct = (data) => {
	console.log(data)

	return Product.findById(data.productId).then((result, error) => {

		console.log(result);

		if(data.isAdmin) {
			result.isActive = false

			console.log(result)

			return result.save().then((archivedProduct, error) => {

				if(error) {

					return false
				}

				else {
					return archivedProduct
				}
			})
		}

		else {
			return "Only admins can archive a product"
		}
	})
}