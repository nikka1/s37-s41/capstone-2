const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");


// 1. User registration
module.exports.registerUser = (reqBody) => {

	let newUser = new User ({
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10)
	})

	return newUser.save().then((user, error) => {

		if(error) {
			return false
		}

		else {
			return true
		}
	})
}

// 2. User authentication
module.exports.loginUser = (reqBody) => {

	return User.findOne({email: reqBody.email}).then(result => {

		if(result == null) {
			return false
		}

		else {

			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if(isPasswordCorrect) {
				return {access: auth.createAccessToken(result)}
			}

			else {
				return false
			}
		}
	})
}

// 3. Set user as Admin
module.exports.setAdmin = (data) => {
	console.log(data)

	return User.findById(data.userId).then((result, error) => {

		console.log(result);

		if(data.isAdmin == false) {
			result.isAdmin = true

			console.log(result)

			return result.save().then((updatedUser, error) => {

				if(error) {
					return false
				}

				else {
					return updatedUser
				}
			})
		}

		else {
			return "User is already an admin"
		}
	})
}