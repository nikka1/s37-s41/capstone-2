const Order = require("../models/Order");
const User = require("../models/User");
const Product = require("../models/Product");

const auth = require("../auth");

// 9. Create order (non-admin)
module.exports.createOrder = (reqBody) => {

	console.log(reqBody)

	let newOrder = new Order ({

		totalAmount: reqBody.totalAmount,
		userId: reqBody.userId,
		products: reqBody.products

	})

	return newOrder.save().then((order, error) => {

		if(error) {
			return false
		}

		else {
			return order
		}
	})
}

// 10. Retrieve all orders
module.exports.getAllOrders = () => {

	return Order.find({}).then(result => {

		return result
	})
}



// 11. Retrieve authenticated user's orders
module.exports.getMyOrders = (data) => {
	console.log(data.userId)

	return Order.find({userId: data.userId}).then(result => {

		return result
	})
}
