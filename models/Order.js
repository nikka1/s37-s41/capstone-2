const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({

	totalAmount: {
		type: Number,
		required: [true, "Total amount is required"]
	},

	userId: {
		type: String,
		required: [true, "User ID is required"]
	},

	products: {
		type: Array,
		default: []
	},


	purchasedOn: {
		type: Date,
		default: new Date()
	}
})

module.exports = mongoose.model("Order", orderSchema);